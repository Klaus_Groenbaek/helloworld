package com.example;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
class MyResponse {
    private String message;
}
