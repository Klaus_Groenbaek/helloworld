package com.example;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    @GetMapping("/")
    public MyResponse sayHello() {
        return new MyResponse().setMessage("Hello World");
    }
}
