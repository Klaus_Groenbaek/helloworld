# Spring Boot Hello World

A simple project that shows how easy it is to setup a Spring boot project 
with gradle.

### Lombok
This project uses lombok, which is an annotation processor which can generate 
setters/getters (and much more) at compile time. To compile this you need to 
enable annotation processors, and your IDE will need a plugin to realise which methods 
are created by during compilation.